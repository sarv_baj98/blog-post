import React from "react";
import { AuthContext } from "../App";

export const Login = () => {
   const { dispatch } = React.useContext(AuthContext);
  const initialState = {
    email: "",
    password: "",
    isSubmitting: false,
    errorMessage: null,
    isLogin:true,
    isSignup:false
  };
  const [data, setData] = React.useState(initialState);
  const handleInputChange = event => {
    setData({
      ...data,
      errorMessage:null,
      [event.target.name]: event.target.value
    });
  };
  const handleClickEvent=event=>{
    event.preventDefault();
    setData({
      ...data,
      isLogin: !data.isLogin,
      isSignup:!data.isSignup,
      errorMessage: null
    });

  }
  const handleSignupFormSubmit = event => {
      event.preventDefault();
      setData({
        ...data,
        isSubmitting: true,
        errorMessage: null
      });
      if(data.email==""|| data.password=="" || data.uname==""){
        setData({
          ...data,
          isSubmitting: false,
          errorMessage:"Please provide information before submit."
        });
      }else{
        fetch("http://localhost:5000/api/signup", {
          method: "POST",
          // mode: "no-cors",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            uname:data.uname,
            password: data.password,
            username: data.email
          })
        })
        .then(res => {
          if (res.ok) {
            return res.json();
          }
          throw res;
        })
        .then(resJson => {
          dispatch({
            type: "LOGIN",
            payload: resJson
          })
        })
        .catch(error => {
          setData({
            ...data,
            isSubmitting: false,
            errorMessage: error.message || error.statusText
          });
        });
      }
    };
  const handleFormSubmit = event => {
    event.preventDefault();
    setData({
      ...data,
      isSubmitting: true,
      errorMessage: null
    });
    if(data.email==""|| data.password==""){
      setData({
        ...data,
        isSubmitting: false,
        errorMessage:"Email address or password field is empty"
      });
    }else{
      fetch("http://localhost:5000/api/login", {
        method: "POST",
        // mode: "no-cors",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          username: data.email,
          password: data.password
        })
      })
      .then(res => {
        if (res.ok) {
          return res.json();
        }
        throw res;
      })
      .then(resJson => {
        dispatch({
          type: "LOGIN",
          payload: resJson
        })
      })
      .catch(error => {
        setData({
          ...data,
          isSubmitting: false,
          errorMessage: error.message || error.statusText
        });
      });
    }
  };
return (
    <div className="login-container">
      <div className="card">
        <div className="container">
        {
          data.isLogin ?
          <form  onSubmit={handleFormSubmit}>
          <h1>Login</h1>

          <label htmlFor="email">
          Email Address
          <input
          type="text"
          value={data.email}
          onChange={handleInputChange}
          name="email"
          id="email"
          />
          </label>

          <label htmlFor="password">
          Password
          <input
          type="password"
          value={data.password}
          onChange={handleInputChange}
          name="password"
          id="password"
          />
          </label>

          {data.errorMessage && (
            <span className="form-error">{data.errorMessage}</span>
          )}

          <button disabled={data.isSubmitting}>
          {data.isSubmitting ? (
            <span className="loader">LOADING...</span>
          ) : (
            "Login"
          )}
          </button>
          <button onClick={handleClickEvent}>
          "Sign Up"
          </button>
          </form> : null
        }
        {
          data.isSignup ?
          <form onSubmit={handleSignupFormSubmit}>
          <h1>Sign Up</h1>
          <label htmlFor="uname">
          Full Name
          <input
          type="text"
          value={data.uname}
          onChange={handleInputChange}
          name="uname"
          id="uname"
          />
          </label>
          <label htmlFor="email">
          Email Address
          <input
          type="text"
          value={data.email}
          onChange={handleInputChange}
          name="email"
          id="email"
          />
          </label>

          <label htmlFor="password">
          Password
          <input
          type="password"
          value={data.password}
          onChange={handleInputChange}
          name="password"
          id="password"
          />
          </label>

          {data.errorMessage && (
            <span className="form-error">{data.errorMessage}</span>
          )}

          <button disabled={data.isSubmitting}>
          {data.isSubmitting ? (
            <span className="loader">LOADING...</span>
          ) : (
            "Sign Up"
          )}
          </button>
          <button onClick={handleClickEvent}>
          "Login"
          </button>
          </form>:null
        }
        </div>
      </div>
    </div>
  );
};
export default Login;
