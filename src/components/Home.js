import React from "react";
import BlogForm from "./BlogForm";
import PostedBlog from "./PostedBlog";
export const Home = () => {
return (
    <div className="home">
      <BlogForm/>
      <PostedBlog/>
    </div>
  );
};
export default Home;
