import React, { Component } from 'react';
import { AuthContext } from "../App";


export const BlogForm = () => {
  const { dispatch } = React.useContext(AuthContext);
  const initialState = {
    title: "",
    description: "",
    errorMessage: null,
    isSubmitting: false

  };
  const [data, setData] = React.useState(initialState);
  const handleInputChange = event => {
      setData({
        ...data,
        [event.target.name]: event.target.value
      });
    };
  const handleFormSubmit=event=>{
    event.preventDefault();
    setData({
      ...data,
      isSubmitting:true,
      errorMessage:null,

    });
    if(data.email==""|| data.password==""){
      setData({
        ...data,
        isSubmitting: false,
        errorMessage:"Cannot submit leaving field empty."
      });
    }else{
      fetch("http://localhost:5000/api/createPost",{
        method:"POST",
        headers:{
          'Authorization':"Bearer "+localStorage.getItem('token'),
          "Content-Type": "application/json"
        },
        body:JSON.stringify({
          title:data.title,
          description:data.description
        })
      })
      .then(res=>{
        if(res.ok){
          return res.json();
        }
        throw res;
      })
      .then(resJson=>{
        dispatch({
          type:"GETPOST",
          payload:resJson
        })
        setData({
          ...data,
          isSubmitting: false,
          errorMessage:""
        });
      })

    }
  }

return (
  <div className="container">
    <h1>Create Post</h1>
    <form>
    <label htmlFor="title">
      Title
      <input
      type="text"
      placeholder="Enter Post Title"
      value={data.title}
      onChange={handleInputChange}
      name="title"
      id="title"
      />
    </label>
    <label htmlFor="description">
      Enter Post
      <textarea
      // type="text"
      value={data.description}
      placeholder="Enter Post detail"
      onChange={handleInputChange}
      rows="5"
      cols="28"
      name="description"
      id="description"
      />
    </label>
    {data.errorMessage && (
      <span className="form-error">{data.errorMessage}</span>
    )}
    <button  onClick={handleFormSubmit} disabled={data.isSubmitting}>
    {data.isSubmitting ? (
      <span className="loader">LOADING...</span>
    ) : (
      "Post"
    )}
    </button>
    </form>
  </div>
);

}
export default BlogForm;
