import React from "react";
import { AuthContext } from "../App";

export const Header = () => {
  const { dispatch } = React.useContext(AuthContext);

  const handleLogoutEvent = event => {
    dispatch({
      type:"LOGOUT"
    })
  }

  return (
    <nav id="navigation">
      <h1 href="#" className="logo">
        Blog Post
      </h1>
      <span onClick={handleLogoutEvent} style={{float: "right",color: "red",cursor: "pointer"}}>Logout </span>

    </nav>
  );
};
export default Header;
