import React from 'react';
import Login from "./components/Login";
import Home from "./components/Home";
import Header from "./components/Headers";
import './App.css';

export const AuthContext = React.createContext();

const initialState = {
  token: localStorage.getItem('token'),
  isAuthenticated: localStorage.getItem('token') ? true : false, // or just !!localStorage.getItem('token')
  user: null,
  posts:null
  // token: null,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("user", JSON.stringify(action.payload.username));
      localStorage.setItem("token", JSON.stringify(action.payload.token));
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.username,
        token: action.payload.token
      };
    case "LOGOUT":
      localStorage.clear();
      return {
        ...state,
        isAuthenticated: false,
        user: null
      };
    case "GETPOST":
    return{
      ...state,
      posts:action.payload

    };

    default:
      return state;
  }
};
function App() {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  return (
    //Authcontext will pass the auth state
    // from this component to any other component that requires it.
    <AuthContext.Provider
      value={{
        state,
        dispatch
      }}
    >
    <Header />
      <div className="App">{!state.isAuthenticated ? <Login /> : <Home />}</div>
    </AuthContext.Provider>
  );
}

export default App;
