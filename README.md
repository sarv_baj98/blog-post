# Blog-post
React JS project for posting blog.

## Run, Build

```bash
$ git clone https://gitlab.com/sarv_baj98/blog-post.git
$ cd blog-post
$ npm install
```

Run the project
```bash
$ npm start
```
## Some Screenshoot
#
![LogIn](https://gitlab.com/sarv_baj98/blog-post/uploads/a152390b0098e76f0d198abd3e61a341/SignIn.PNG)
#
![Signup](https://gitlab.com/sarv_baj98/blog-post/uploads/54ee1e987f4de620cfa048cb201e97c2/SignUP.PNG)
#
![Createpost](https://gitlab.com/sarv_baj98/blog-post/uploads/335997279df355e05d28d760bede7bc3/Createpost.PNG)
